<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the `social_Posts` table
		Schema::create('social_posts', function($table)
		{
            $table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned()->index();
			$table->integer('social_media_id');
			$table->string('title')->nullable();
			//$table->string('slug');
			$table->text('content');
			$table->string('tags')->nullable();
			$table->string('image_path')->nullable();
			//$table->string('meta_keywords');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `social_posts` table
		Schema::drop('social_posts');
	}

}
