<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('email');
            $table->string('screen_name');
            $table->string('signinType');
            $table->string('social_media_id');
            $table->string('social_media_type');
            $table->string('name');
            $table->string('profile_picture');
            $table->string('location');
            $table->string('oauth_token');
            $table->string('oauth_secret');
            $table->string('app_oauth_token');
            $table->string('app_oauth_secret');            
            $table->string('x_auth_expires');
            $table->timestamps();
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
