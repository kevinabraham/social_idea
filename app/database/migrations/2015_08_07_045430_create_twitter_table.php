<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			// Create the `Twitter` table
					Schema::create('twitter', function($table)
					{
			            $table->engine = 'InnoDB';
						$table->increments('id')->unsigned();
						$table->integer('user_id')->unsigned()->index();
						$table->string('screen_name');
						$table->text('tweet_text');
						$table->string('posted_by');
						$table->string('meta_description');
						$table->string('tweet_link');
						$table->timestamps();
						//$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
					});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Delete the `Twitter` table
		Schema::drop('twitter');
	}

}
