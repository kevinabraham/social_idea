<?php

class Tweet extends Eloquent {

    protected $guarded = array();

 

    public static $rules = array(

        'body' => 'required'

    );

 

    public function tags() {

        return $this->belongsToMany('App\Models\Tweet');

    }

}
