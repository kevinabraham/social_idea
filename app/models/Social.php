<?php

class Social extends Eloquent {


    /**
     * Get user's profile picture
     * @param $profile_picture
     * @return mixed
     */
        public static function getTwitterProfilePicture()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('profile_picture');
    }

    /**
     * Get user's profile picture
     * @param $profile_picture
     * @return mixed
     */
        public static function getAnyProfilePicture()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('profile_picture');
    }

    /**
     * Get user's profile picture
     * @param $profile_picture
     * @return mixed
     */
        public static function getTweets()
    {
        return Twitter::getUserTimeline(array('screen_name' => DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('screen_name'), 'count' => 10, 'format' => 'object'));
    }

     /**
     * Get user's profile picture
     * @param $profile_picture
     * @return mixed
     */
        public static function getTwitterScreenName()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('screen_name');
    }

    /**
     * Get user's profile picture
     * @param $twitter_profile
     * @return mixed
     */
        public static function getTwitterProfile()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->first();
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getTwitterName()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('name');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getSocialProfile()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->get();
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getSocialMediaID()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('social_media_id');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getSocialMediaType()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('social_media_type');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getOauthToken()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('oauth_token');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getOauthSecret()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('oauth_secret');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getXauthExpires()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('x_auth_expires');
    }


    /**
     * Get unique user's profile data
     ******************************************************************************************************************************************************************
     */

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueSocialMediaID()
    {
        return Input::get('social_media_id');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueTwitterName()
    {
        return DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('name');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueTwitterProfilePicture()
    {
        return DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('profile_picture');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueSocialProfile()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->get();
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueScreenName()
    {
        return DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('screen_name');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueMainProfilePicture()
    {
        return DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('profile_picture');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueOauthToken()
    {
        return DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('oauth_token');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueOauthSecret()
    {
        return DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('oauth_secret');
    }

    /**
     * Get user's profile picture
     * @param $twitter_name
     * @return mixed
     */
        public static function getUniqueXauthExpiry()
    {
        return DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('x_auth_expires');
    }

   









}