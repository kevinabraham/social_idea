<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
}
</style>
</head>
<body>

 foreach($tweets as $tweet){
        echo '<b>Tweet Text:</b> '.Twitter::linkify($tweet->text).'<br>';
        echo '<strong>Posted By:</strong> <a href="http:'.Twitter::linkUser($tweet->user).
        '">'.$tweet->user->name.'</a> <em>'.Twitter::ago($tweet->created_at).'</em><br>';
        echo '<strong>Original Tweet:</strong> <a href="http:'.Twitter::linkTweet($tweet).
        '">http:'.Twitter::linkTweet($tweet).'</a>';

</body>
</html>
