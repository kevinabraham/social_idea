
                                        <div class="lv-avatar pull-left">
                                            <img src="{{ $profile_picture }}" alt="">
                                        </div>
                                        <span class="c-black">{{ '@' . $social_profile->screen_name }}</span>
                                    </div>
                                    
                                    <ul class="lv-actions actions">
                                        <li>
                                            <a href="#">
                                                <i class="md md-delete"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="md md-check"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="md md-access-time"></i>
                                            </a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" aria-expanded="true">
                                                <i class="md md-sort"></i>
                                            </a>
                                
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="#">Latest</a>
                                                </li>
                                                <li>
                                                    <a href="#">Oldest</a>
                                                </li>
                                            </ul>
                                        </li>                             
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown" aria-expanded="true">
                                                <i class="md md-more-vert"></i>
                                            </a>
                                
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="#">Refresh</a>
                                                </li>
                                                <li>
                                                    <a href="#">Message Settings</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="lv-body"> 


 <div class="card">
                        
                        
                        <div class="card-body card-padding">
                            <div role="tabpanel">
                                <ul class="tab-nav" role="tablist">
                                    <li class="active"><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab">Content</a></li>
                                    <li><a href="#profile11" aria-controls="profile11" role="tab" data-toggle="tab">Analytics</a></li>
                                    <li><a href="#messages11" aria-controls="messages11" role="tab" data-toggle="tab">Streams</a></li>
                                    <li><a href="#settings11" aria-controls="settings11" role="tab" data-toggle="tab">Settings</a></li>
                                </ul>
                              
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home11">
                                         <div class="card">
                       
                        
                        <!--<div class="card-body card-padding">-->
                            
                            <br/>


            @foreach ($tweet as $tweet)

                            
                            <div class="media-demo">
                                <div class="media">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img class="media-object" src="{{ $tweet->user->profile_image_url }}" alt="">
                                        </a>
                                    </div>

                                  
    


                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="http:'{{ Twitter::linkUser($tweet->user) }}'">{{ $tweet->user->name }}</a></h4>
                                        {{ Twitter::linkify($tweet->text) }}
                                        <ul class="lv-attrs">
                                            <li>Posted: {{ Twitter::ago($tweet->created_at) }}</li>
                                            <li>Retweets: {{ Twitter::linkify($tweet->retweet_count) }}</li>
                                            <li>Favourites: {{ Twitter::linkify($tweet->favorite_count) }}</li>
                                            <li>Reach: {{ Twitter::linkify($tweet->user->friends_count) }}</li>
                                        </ul>
                                    </div>
                                </div>
                                <br>
                             



                              @endforeach  





                                    
                                    
                                  
                                   
                                    
                                 
