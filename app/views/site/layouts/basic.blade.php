<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
    
        <!-- Vendor CSS -->
        {{ HTML::style('theme/vendors/bower_components/animate.css/animate.min.css') }}
        {{ HTML::style('theme/vendors/bower_components/sweetalert/dist/sweetalert-override.min.css') }}
        {{ HTML::style('theme/vendors/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css') }}
        {{ HTML::style('theme/vendors/socicon/socicon.min.css') }}
            
        <!-- CSS -->
        {{ HTML::style('theme/css/app.min.1.css') }}
        {{ HTML::style('theme/css/app.min.2.css') }}
        {{ HTML::style('theme/vendors/nprogress/nprogress2.css') }}

    </head>
    
    <body>
        <header id="header">
            <ul class="header-inner">
                
            
                <li class="logo hidden-xs">
                    <a href="{{ URL::to('/') }}">&nbsp&nbsp&nbsp&nbspUnify</a>
                </li>
                
         </ul>
         </header>
                
  @yield('content')                  
        
        

        </section>
        
        <footer id="footer">
            Copyright &copy; 2015 Unify
            
            <ul class="f-menu">
                <li><a href="{{ URL::to('/') }}">Home</a></li>
                <li><a href="{{ URL::to('/pricing') }}">Pricing</a></li>
                <li><a href="{{ URL::to('/terms') }}">Terms</a></li>
                <li><a href="{{ URL::to('/privacy') }}">Privacy</a></li>
                <li><a href="mailto:support@tryunify.com?Subject=Hello%20again">Contact</a></li>
            </ul>
        </footer>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
    
        <!-- Javascript Libraries -->
         {{ HTML::script('theme/vendors/bower_components/jquery/dist/jquery.min.js') }}
         {{ HTML::script('theme/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}
         {{ HTML::script('theme/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js') }}  
         {{ HTML::script('theme/vendors/bower_components/Waves/dist/waves.min.js') }}
         {{ HTML::script('theme/vendors/bootstrap-growl/bootstrap-growl.min.js') }}
         {{ HTML::script('theme/vendors/bower_components/sweetalert/dist/sweetalert.min.js') }}      
         {{ HTML::script('theme/vendors/bower_components/autosize/dist/autosize.min.js') }}           
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            {{ HTML::script('theme/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js') }}
        <![endif]-->
        

        {{ HTML::script('theme/js/functions.js') }}
       <!--{{ HTML::script('theme/js/demo.js') }}-->
        {{ HTML::script('theme/vendors/nprogress/nprogress.js') }}
        <script type="text/javascript">
          $('body').show();
            $('.version').text(NProgress.version);
            NProgress.start();
            setTimeout(function() { NProgress.done(); $('.fade').removeClass('out'); }, 1000);
        </script>
    
    
    </body>

</html>