<!DOCTYPE html>
<!--[if lte IE 9]> <html class="ghost-org no-js old-ie "> <![endif]-->
<!--[if gt IE 9]> <html class="ghost-org no-js "> <!--<![endif]-->
<!--[if !IE]> --> <html class="ghost-org "> <!-- <![endif]-->


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
<script type="text/javascript" src="ruxitagentjs_2fnqr_10073150625174103.js" data-dtconfig="rid=RID_2418|rpid=1269403827|domain=tryunify.org|reportUrl=/rb_orm84958|tp=500,50,0"></script><script src="https://cdn.optimizely.com/js/654983416.js"></script>
<title>Unify - Terms Unify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus.</title>
<meta name="description" content="nify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus."/>
<meta property="og:description" content="nify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus."/>
<meta property="twitter:description" content="nify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus.">
<meta property="twitter:card" content="summary"/>
<meta property="twitter:title" content="Unify - Social media analytics made simple">
<meta property="twitter:site" content="@tryunify">
<meta property="twitter:domain" content="http://tryunify.com"/>
<meta property="twitter:url" content="http://tryunify.com"/>
<meta property="og:site_name" content="Unify"/>
<meta property="fb:admins" content="511697361"/>
<meta property="og:title" content="Unify - Invoicing made simple"/>
<meta property="og:url" content="http://tryunify.com"/>
<meta name="HandheldFriendly" content="True"/>
<meta name="MobileOptimized" content="320"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="cleartype" content="on"/>
<link rel="shortcut icon" type="image/x-icon" href="home/assets/favicon-e13fe17b11ad125601924cfb260f20da.png?v=2"/>
<link rel="canonical" href="index.php"/>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:700,600,400,300"/>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Noto+Serif"/>
<link data-turbolinks-track="true" href="home/assets/style.css" media="all" rel="stylesheet"/>
{{ HTML::style('theme/vendors/nprogress/nprogress.css') }}

<!-- Google Tracking Code for www.tryunify.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65303751-1', 'auto');
  ga('send', 'pageview');

</script>


<!-- Hotjar Tracking Code for www.tryunify.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:51458,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

</head>
<body class="page" data-logged-in-user=''>
<header id="global-header" class="global-header navbar">
<nav role="navigation">
<div class="wrapper">
<a class="" href="{{ URL::to('/') }}"><font size="10px" color="#14B5EA"><i class="fa fa-code-fork"></i> </font></a>
<button type="button" class="mobile-menu" data-toggle="collapse" data-target=".navbar-collapse"><span class="hidden">Menu</span></button>
<ul id="main-menu" class="navbar-collapse collapse">
<li class="home-button-container"><a class="" href="{{ URL::to('/') }}">Home</a></li>
<li><a class="" href="{{ URL::to('/pricing') }}">Pricing</a></li>
<li><li class='current-parent'><a class="" href="{{ URL::to('/about') }}">About</a></li></li>
<li><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again" target="_top">Support</a></li>
<li class="login-button-container"><a class="login" href="{{ URL::to('/user/create') }}" rel="nofollow">Sign up</a></li>
</ul>
</div>
</nav>
</header>



@yield('content')



<div class="clearfix"></div>
<footer id="global-footer">
<nav id="footer-nav" role="navigation">
<ul id="footer-menu">
<!--<li class="logo"><a href="#"><img alt="Unify" src="assets/logo.png"/></a></li>-->
<li><a class="" href="{{ URL::to('/about') }}">About</a></li>
<li><a class="" href="{{ URL::to('/pricing') }}">Pricing</a></li>
<li><a class="" href="{{ URL::to('/terms') }}">Terms</a></li>
<li><a class="" href="{{ URL::to('/privacy') }}">Privacy</a></li>
<li><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again">Contact</a></li>
</ul>
<div class="clearfix"></div>
<span class="poweredby"><img alt="Unify" src="home/assets/logo.png"/> All rights reserved. <em>© 2015</em> <a href="http://tryunify.com" target="_blank">Unify</a></span>
<ul id="social-menu">
<li class="github"><a href="#"><span class="hidden">Github</span></a></li>
<li class="twitter"><a href="https://twitter.com/charge_loop"><span class="hidden">Twitter</span></a></li>
<li class="gplus"><a href="#"><span class="hidden">Google+</span></a></li>
<li class="facebook"><a href="#"><span class="hidden">Facebook</span></a></li>
</ul>
</nav>
<div class="clearfix"></div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--[if lte IE 9]>
      <script src="/assets/vendor/flexie.min.js"></script>
      <![endif]-->
<script src="home/assets/application-f70a08abb1fd9fbfdd4ae25ddd2c3d6b.js"></script>
{{ HTML::script('theme/vendors/nprogress/nprogress.js') }}
<script type="text/javascript">
  $('body').show();
    $('.version').text(NProgress.version);
    NProgress.start();
    setTimeout(function() { NProgress.done(); $('.fade').removeClass('out'); }, 1000);
</script>
</body>

</html>
