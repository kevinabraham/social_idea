<!DOCTYPE html>
<!--[if lte IE 9]> <html class="ghost-org no-js old-ie "> <![endif]-->
<!--[if gt IE 9]> <html class="ghost-org no-js "> <!--<![endif]-->
<!--[if !IE]> --> <html class="ghost-org "> <!-- <![endif]-->


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
<script type="text/javascript" src="ruxitagentjs_2fnqr_10073150625174103.js" data-dtconfig="rid=RID_2418|rpid=1269403827|domain=tryunify.org|reportUrl=/rb_orm84958|tp=500,50,0"></script><script src="https://cdn.optimizely.com/js/654983416.js"></script>
<title>Unify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus.</title>
<meta name="description" content="Unify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus."/>
<meta property="og:description" content="nify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus."/>
<meta property="twitter:description" content="nify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus.">
<meta property="twitter:card" content="summary"/>
<meta property="twitter:title" content="Unify - Social media analytics made simple">
<meta property="twitter:site" content="@tryunify">
<meta property="twitter:domain" content="http://tryunify.com"/>
<meta property="twitter:url" content="http://tryunify.com"/>
<meta property="og:site_name" content="Unify"/>
<meta property="fb:admins" content="511697361"/>
<meta property="og:title" content="Unify - Invoicing made simple"/>
<meta property="og:url" content="http://tryunify.com"/>
<meta name="HandheldFriendly" content="True"/>
<meta name="MobileOptimized" content="320"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="cleartype" content="on"/>
<link rel="shortcut icon" type="image/x-icon" href="home/assets/favicon-e13fe17b11ad125601924cfb260f20da.png?v=2"/>
<link rel="canonical" href="index.php"/>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:700,600,400,300"/>
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Noto+Serif"/>
<link data-turbolinks-track="true" href="home/assets/style.css" media="all" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!--[if lte IE 9]>
      <script src="/assets/vendor/flexie.min.js"></script>
      <![endif]-->
<script src="home/assets/application-f70a08abb1fd9fbfdd4ae25ddd2c3d6b.js"></script>

<!-- Google Tracking Code for www.tryunify.com -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65303751-1', 'auto');
  ga('send', 'pageview');

</script>


<!-- Hotjar Tracking Code for www.tryunify.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:51458,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<meta content="authenticity_token" name="csrf-param"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

</head>
<body class="page full-width" data-logged-in-user=''>
<header id="global-header" class="global-header navbar">
<nav role="navigation">
<div class="wrapper">
<a class="" href="https://tryunify.com/"><font size="8px" color="#14B5EA"><i class="fa fa-code-fork"></i> </font></a>
<button type="button" class="mobile-menu" data-toggle="collapse" data-target=".navbar-collapse"><span class="hidden">Menu</span></button>
<ul id="main-menu" class="navbar-collapse collapse">
<li class="home-button-container"><a class="" href="{{ URL::to('/') }}">Home</a></li>
<li><a class="" href="{{ URL::to('/pricing') }}">Pricing</a></li>
<li><li class='current-parent'><a class="" href="{{ URL::to('/about') }}">About</a></li></li>
<li><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again" target="_top">Support</a></li>
<li class="login-button-container"><a class="login" href="{{ URL::to('/user/create') }}" rel="nofollow">Sign up</a></li>
</ul>
</div>
</nav>
</header>


<div class="clearfix"></div>
<main role="main">
<section class="pricing-wrapper page-wrapper">
<h1 class="pricing-headline">Understand your audience.</h1>
<h4 class="pricing-subheader">Comming soon. <a class=" current" href="{{ URL::to('/user/create') }}">Sign up for early access</a></h4>
<section class="pricing-table">

<div class="paid-plans-wrapper">


<span class="word"> </span>

<div class="version-pill green"><span>Unify - Comming soon</span></div>
</div>
<div class="paid-plans top">
<article class="plan">
<header class="plan-header light-green-bg">
<span class="uppercase plan-name">Personal</span>
<h3><sup>$</sup>10</h3>
<span class="plan-per-month">per month</span>
<div class="plan-explain">
<span class="text">Billed annually, or $12 month-to-month</span>
</div>
</header>
<div class="features">
<ul>
<li class="plan-views">
<b>Up to 5 social profiles.</b>
<div class="plan-explain">
<span class="text">A social profile is a connected social media or other 3rd party app which integrates into Unify. If you need more social profiles just bump up to the next plan.</span>
</div>
</li>
<li><b>1 user</b></li>
<li>Enhanced Analytics</li>
<li>Message Scheduling</li>
<li>RSS feed integration</li>
<li>Link analytics</li>
</ul>
<p><b>In Development &amp; Coming Soon</b></p>
<ul>
<li>3rd party apps</li>
<li>Business analytics</li>
</ul>
</div>
</article>
<article class="plan big">
<header class="plan-header med-green-bg">
<span class="uppercase plan-name">Advanced</span>
<h3><sup>$</sup>25</h3>
<span class="plan-per-month">per month</span>
<div class="plan-explain">
<span class="text">Billed annually, or $30 month-to-month</span>
</div>
</header>
<div class="features">
<ul>
<li class="plan-views">
<b>Up to 25 social profiles</b>
<div class="plan-explain">
<span class="text">A social profile is a connected social media or other 3rd party app which integrates into Unify. If you need more social profiles just bump up to the next plan.</span>
</div>
</li>
<li><b>5 users</b></li>
<li>Enhanced Analytics</li>
<li>Message Scheduling</li>
<li>RSS feed integration</li>
<li>Link analytics</li>
</ul>
<p><b>In Development &amp; Coming Soon</b></p>
<ul>
<li>3rd party apps</li>
<li>Vanity URL's</li>
<li>Full SSL Support</li>
<li>Developer tools</li>
</ul>
</div>
</article>
<article class="plan">
<header class="plan-header light-green-bg">
<span class="uppercase plan-name">Pro</span>
<h3><sup>$</sup>80</h3>
<span class="plan-per-month">per month</span>
<div class="plan-explain">
<span class="text">Billed annually, or $100 month-to-month</span>
</div>
</header>
<div class="features">
<ul>
<li class="plan-views">
<b>Up to 100 social profiles</b>
<div class="plan-explain">
<span class="text">A social profile is a connected social media or other 3rd party app which integrates into Unify. If you need more social profiles just bump up to the next plan.</span>
</div>
</li>
<li><b>25 users</b></li>
<li>Enhanced Analytics</li>
<li>Message Scheduling</li>
<li>RSS feed integration</li>
<li>Link analytics</li>
<li>Enhanced support</li>
</ul>
<p><b>Bonus features</b></p>
<ul>
<li>3rd party apps</li>
<li>Post analytics</li>
<li>Full SSL Support</li>
<li>Developer tools</li>
<li>Turbo performance</li>
</ul>
</div>
</article>
<article class="plan">
<header class="plan-header wash-green-bg">
<span class="uppercase plan-name">Enterprise</span>
<h3><sup>$</sup>200</h3>
<span class="plan-per-month">per month</span>
<div class="plan-explain">
<span class="text">Billed annually, or $250 month-to-month</span>
</div>
</header>
<div class="features">
<ul>
<li class="plan-views">
<b>Unlimited social profiles</b>
<div class="plan-explain">
<span class="text">A social profile is a connected social media or other 3rd party app which integrates into Unify. If you need more social profiles just bump up to the next plan.</span>
</div>
</li>
<li><b>Unlimited users</b></li>
<li></li>
<li>Enhanced Analytics</li>
<li>Message Scheduling</li>
<li>RSS feed integration</li>
<li>Link analytics</li>
<li>Dedicated account rep</li>
<li>Venity URL's</li>
<li>Compliance integration</li>
</ul>
<p><b>Bonus features</b></p>
<ul>
<li>3rd party apps</li>
<li>Post analytics</li>
<li>Full SSL Support</li>
<li>Developer tools</li>
<li>Turbo performance</li>
</ul>
</div>
</article>
</div>
<div class="plan-in-dev">
<p><b>Bonus features</b></p>
</div>
<div class="paid-plans bottom">
<article class="plan">
<ul>
<li>3rd party apps</li>
<li></li>
</ul>
</article>
<article class="plan big">
<ul>
<li>3rd party apps</li>
<li>Developer tools</li>
</ul>
</article>
<article class="plan">
<ul>
<li>3rd party apps</li>
<li>Developer tools</li>
<li>Full SSL Support</li>
</ul>
</article>
<article class="plan">
<ul>
<li>3rd party apps</li>
<li>Developer tools</li>
<li>Full SSL Support</li>
<li>Turbo performance</li>
</ul>
</article>
</div>
</div>
</section>
<div class="button-divider">
<a href="http://tryunify.com" class="button-add large">Sign up to get early access!</a>
</div>
<section class="faq" id="billing-faq">
<article>
<h3>Why should I use Unify?</h3>
<p>Great question. "It can be hard for a small business to be able to manage all thier marketing plus figure out which channel is working the best." is a direct quote from one of our users. This is the reason behind Unify. Its designed to help you manage all your marketing and provide you with the simplest analytics so you can make better marketing decisions.</p>
</article>
<article>
<h3>Many payments are annoying! Can I be billed annually?</h3>
<p>Yes you can! Not only that, but we'll give you a <mark>20% discount</mark> if you decide to pay for a year in advance. Success.</p>
</article>
<article>
<h3>How are "social profiles" calculated?</h3>
<p>A social profile is simply a connected 3rd party social media website or other app such as google analytics.</p>
</article>
<article>
<h3>What happens if I hit my social pofile limit limit?</h3>
<p>Good job sounds like your business is growing well! If this happens we'll email you and ask you to upgrade or you can simply bump up to the next plan.</p>
</article>
<article>
<h3>I have custom needs. What can I do?</h3>
<p>Get in touch with us and we'll put together a custom plan just for you.</p>
</article>
<article>
<h3>Can I use my own domain name for url tracking?</h3>
<p>Yes! If you are on the enterprise or pro plan. You can use any domain you like and point it at your tryunify.com in the space of a few minutes.</p>
</article>
<article>
<h3>What payment methods do you accept?</h3>
<p>Currently we accept all major credit cards. All our processing is done via stripe so you credit card data is in safe hands at all times and never actually gets stored in our servers. </p>
</article>
<article>
<h3>Are there different fees for different countries?</h3>
<p>As we use paypal and stripe as a backend your payments will automatically be converted to your home currency. We do not charge you any fees on top. The only fees are charged by paypal and stripe directly according to whichever plan you have.</p>
</article>
<article>
<h3>I have ideas for more features, Can I get in touch?</h3>
<p>Awesome! We would love to hear from you, Our development team is always looking for new ideas to improve your experience. Just shoot us an email at <a href="mailto:support@etryunify.com?Subject=Feature%20Request" target="_top">support@tryunify.com</a>.</p>
</article>
<article>
<h3>I have more questions!</h3>
<p>Our support team is on hand and ready to help, and we usually reply pretty quick! <a href="mailto:support@etryunify.com?Subject=Hello%20again" target="_top">Click here to email us!</a>.</p>
</article>
</section>
<div class="button-divider">
<a href="{{ URL::to('/guidelines') }}" class="button-add large">Sign up for early access.</a>
</div>
</section>
</main>


<div class="clearfix"></div>
<footer id="global-footer">
<nav id="footer-nav" role="navigation">
<ul id="footer-menu">
<!--<li class="logo"><a href="#"><img alt="Unify" src="assets/logo.png"/></a></li>-->
<li><a class="" href="{{ URL::to('/about') }}">About</a></li>
<li><a class="" href="{{ URL::to('/pricing') }}">Pricing</a></li>
<li><a class="" href="{{ URL::to('/terms') }}">Terms</a></li>
<li><a class="" href="{{ URL::to('/privacy') }}">Privacy</a></li>
<li><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again" target="_top">Contact</a></li>
</ul>
<div class="clearfix"></div>
<span class="poweredby"><img alt="Unify" src="home/assets/logo.png"/> All rights reserved. <em>© 2015</em> <a href="http://tryunify.com" target="_blank">Unify</a></span>
<ul id="social-menu">
<li class="github"><a href="#"><span class="hidden">Github</span></a></li>
<li class="twitter"><a href="http://twitter.com/charge_loop"><span class="hidden">Twitter</span></a></li>
<li class="gplus"><a href="#"><span class="hidden">Google+</span></a></li>
<li class="facebook"><a href="#"><span class="hidden">Facebook</span></a></li>
</ul>
</nav>
<div class="clearfix"></div>
</footer>
<script src="home/assets/analytics_foot-9e54362f40f41f047f6d7cd53aaca57a.js"></script>
</body>

</html>
