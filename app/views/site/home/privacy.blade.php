@extends('site.home.layout')  
@section('content')
<div class="clearfix"></div>
<main role="main">
<section class="page-wrapper">
<nav class="page-nav">
<ul>
<li class=''><a class="" href="{{ URL::to('/about') }}">Overview</a></li>
<li class=''><a class="" href="{{ URL::to('/guidelines') }}">Guidelines</a></li>
<li class=''><a class="" href="{{ URL::to('/trademark') }}">Trademark Policy</a></li>
<li class='current current-parent'><a class="current current-parent" href="{{ URL::to('/privacy') }}">Privacy Policy</a></li>
<li class=''><a class="" href="{{ URL::to('/terms') }}">Terms of Service</a></li>
<li class=''><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again" target="_top">Contact</a></li>
</ul>
</nav>
<article class="page-content">
<h1>Privacy Policy</h1>
<p>As with many websites, we collect the e-mail addresses of those who communicate with us via e-mail, aggregate information on what pages consumers access or visit, and information volunteered by the user. The information we collect is used to improve the content of our web pages and the quality of our service and is never shared with or sold to other organizations for commercial purposes, except to provide products or services you've requested, when we have your permission, or if it is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our Terms of Service, or as otherwise required by law.</p>
<h3><a name="information-gathering-and-usage" href="#information-gathering-and-usage"></a>Information Gathering and Usage</h3>
<ul>
<li>When you register on tryunify.com we ask for information such as your name, email address, billing address, credit card information. Members who sign up for the free account are not required to enter a credit card.</li>
<li>Unify uses collected information for the following general purposes: products and services provision, billing, identification and authentication, services improvement, contact, and research.</li>
</ul>
<h3><a name="cookies" href="#cookies"></a>Cookies</h3>
<ul>
<li>A cookie is a small amount of data, which often includes an anonymous unique identifier, that is sent to your browser from a web site's computers and stored on your computer's hard drive.</li>
<li>Cookies are required to use the tryunify.com service.</li>
<li>We use cookies to record current session information, but do not use permanent cookies. You are required to re-login to your tryunify.com account after a certain period of time has elapsed to protect you against others accidentally accessing your account contents.</li>
</ul>
<h3><a name="data-storage" href="#data-storage"></a>Data Storage</h3>
<p>Unify uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run tryunify.com. Although Unify owns the code, databases, and all rights to the tryunify.com website, you retain all rights to your data.</p>
<h3><a name="disclosure" href="#disclosure"></a>Disclosure</h3>
<p>Unify may disclose personally identifiable information under special circumstances, such as to comply with subpoenas or when your actions violate the Terms of Service.</p>
<h3><a name="data-location" href="#data-location"></a>Data Location</h3>
<p>If you choose to provide tryunify.com with your information, you consent to the transfer and storage of that information on our servers located in the United States and Australia.</p>
<h3><a name="changes" href="#changes"></a>Changes</h3>
<p>We may periodically update this policy. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your tryunify.com primary account holder information or by placing a prominent notice on our site.</p>
<h3><a name="questions" href="#questions"></a>Questions</h3>
<p>Any questions about this Privacy Policy should be addressed to support [at] tryunify.com.</p>
</article>
</section>

@stop