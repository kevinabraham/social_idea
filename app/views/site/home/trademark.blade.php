@extends('site.home.layout')  
@section('content')

<div class="clearfix"></div>
<main role="main">
<section class="page-wrapper">
<nav class="page-nav">
<ul>
<li class=''><a class="" href="{{ URL::to('/about') }}">Overview</a></li>
<li class=''><a class="" href="{{ URL::to('/guidelines') }}">Guidelines</a></li>
<li class='current current-parent'><a class="" href="{{ URL::to('/trademark') }}">Trademark Policy</a></li>
<li class=''><a class="" href="{{ URL::to('/privacy') }}">Privacy Policy</a></li>
<li class=''><a class="" href="{{ URL::to('/terms') }}">Terms of Service</a></li>
<li class=''><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again" target="_top">Contact</a></li>
</ul>
</nav>
<article class="page-content">
<h1>Trademark Usage Policy</h1>
<p>The word "Unify" and the Unify Logo are trademarks owned by Unify. We would like to extend fair usage of our trademarks as widely as possible, so we have created the following guidelines to help people understand what is (and isn't) ok.</p>
<h2>Permission for Use</h2>
<p>Written permission from The Unify is required to use the Unify name or logo as any part of your project or associated assets. Please contact us on info at Unify.com if you would like to enquire about permission for use.</p>
<p>The purpose of a trademark is not to prevent anyone else in the world from using it, it is to prevent confusion amongst consumers as to which brand is the "official" one. Our primary criteria for the approval or denial of trademark usage requests is whether or not people might confuse it for an official Unify project.</p>
<h2>Automatic Approval</h2>
<p>In some special cases, we will grant automatic permission for a project to use the Unify trademark. Explicit permission to use the trademark is not required when a project meets the following criteria:</p>
<ol>
<li>
You exclusively use the Unify trademark to either extend or improve the Unify software, or to encourage the use of the Unify software (in short &mdash; "foster the Unify software" ).
<ul>
<li><strong>Ok:</strong> An open access, free monthly newsletter called "Unify Weekly"</li>
<li><strong>Not Ok:</strong> A Unify fork called "A Non-Sucky Version of Unify"</li>
<li><strong>Not Ok:</strong> Selling tshirts with the Unify Logo to raise money for another cause</li>
</ul>
</li>
<li>
The Unify trademark is used in a domain name, title of website, seminar, software package, book, magazine or video that is exclusively intended to foster the Unify software and does not suggest an "official link".
<ul>

<li><strong>Automatic Approval:</strong> A keynote called <em>Getting Started with Unify</em></li>
<li><strong>Requires Permission:</strong> A book titled <em>The Official Unify Guide</em></li>
<li><strong>Requires Permission:</strong> A commercial website called <em>Unify.com</em></li>
</ul>
</li>
<li>
The Unify trademark is used for the title of a camp or meet-up, but not in combination with the words: conference, conf, convention or foundation.
<ul>
<li><strong>Ok:</strong> Unify Annual Meetup 2015</li>
<li><strong>Not Ok:</strong> UnifyConf London</li>
</ul>
</li>
<li>
You want to display the official Unify Logo (whether for commercial or non-commercial use) in a standalone and unaltered form.
<ul>
<li><strong>Ok:</strong> Putting the Unify Logo on your product packaging</li>
<li><strong>Not Ok:</strong> Changing the colours or shape of the Unify Logo for your purposes</li>
</ul>
</li>
<li>
You want to use the Unify trademark to describe your product or services without implying any kind of official link to The Unify or the Unify open source project.
<ul>
<li><strong>Ok:</strong> referring to your services as <em>specialising in Unify development</em></li>
<li><strong>Not Ok:</strong> referring to yourself as <em>The Unify Consultant</em></em></li>
</ul>
</li>
</ol>
<h2>Automatic Restriction</h2>
<p>In some specific cases, you will always require explicit permission from The Unify in order to use the Unify trademark. Even if you meet the criteria for automatic approval, you must always seek permission for trademark use if your project meets any of the following criteria:</p>
<ol>
<li>
The use of the Unify trademark suggests an "official link" between your product or service and The Unify. For example:
<ul>
<li>A domain called <em>officialUnifyfaq.org</em></li>
<li>A company called <em>Unify Development Ltd</em></li>
<li>A website titled <em>Unify Support Agency</em></li>
</ul>
</li>
<li>
The Unify trademark is used as part of a "Unify.tld" domain name. For example:
<ul>
<li>Unify.ly</li>
<li>Unify.info</li>
<li>Unify.eu</li>
</ul>
</li>
<li>
The Unify trademark is used as part of a domain name that covers an entire category of products or services that are relevant to the Unify community. For example:
<ul>
<li>Unify-Themes.com</li>
<li>Unify-Plugins.org</li>
<li>Unify-Tshirts.us</li>
<li>Unify-Mobile.co.uk</li>
<li>Unify-Magazine.eu</li>
<li>Unify-Hosting.net</li>
</ul>
</li>
<li>
The Unify trademark is used as part of the name of a company, organisation, trade name or association. For example:
<ul>
<li>Unify Users Germany</li>
<li>Unify Inc</li>
<li>Unify Bloggers Association</li>
</ul>
</li>
<li>
You would like to use the Unify trademark as a part of an advertising campaign. For example:
<ul>
<li>Google Adwords/Adsense</li>
<li>Facebook promoted posts</li>
<li>A television advertisement</li>
</ul>
</li>
</ol>
<p>If you're not sure whether your use qualifies for automatic approval or not, please ask!</p>
<h2>Applying for Permission</h2>
<p>If you would like to apply for permission to use the Unify trademark based on the guidelines above, please email us directly on info at Unify.com including your full details and the details of your requested use.</p>
<p>Your application's approval will be considered based primarily on whether the product or service in question...</p>
<ul>
<li>suggest an offical link or could lead to confusion about being an official project</li>
<li>promotes Unify usage around the world</li>
<li>is released under an open source license compatible with Unify's license</li>
<li>is created by significant contributors to the Unify open source project</li>
<li>has a positive impact on the overall Unify community</li>
</ul>
<h2>Rules of Usage</h2>
<p>With the exception of the "nominative fair use" of the Unify trademark, your use of the trademark is subject to the following rules (irrespective of whether you received automatic or explicit permission for use):</p>
<ol>
<li>Any use of the Unify trademark indicates acceptance of this policy.</li>
<li>The Unify trademark cannot be used for illegal, defamatory or humiliating purposes, or any other purpose that may negatively impact the Unify software or The Unify.</li>
<li>Wherever possible, the Unify trademark should be accompanied by the following text (or an appropriate translation): <em>Unify is a trademark of The Unify.</em></li>
</ol>
<p>These trademark rules and guidelines are subject to change at any time. It is your responsibility to check this agreement periodically for changes.</p>
</article></section>

@stop