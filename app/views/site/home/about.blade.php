@extends('site.home.layout')  
@section('content')

<div class="clearfix"></div>
<main role="main">
<section class="page-wrapper">
<nav class="page-nav">
<ul>
<li class='current current-parent'><a class="current-parent" href="{{ URL::to('/about') }}">Overview</a></li>
<li class=''><a class="" href="{{ URL::to('/guidelines') }}">Guidelines</a></li>
<li class=''><a class="" href="{{ URL::to('/trademark') }}">Trademark Policy</a></li>
<li class=''><a class="" href="{{ URL::to('/privacy') }}">Privacy Policy</a></li>
<li class=''><a class="" href="{{ URL::to('/terms') }}">Terms of Service</a></li>
<li class=''><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again" target="_top">Contact</a></li>
</ul>
</nav>
<article class="page-content">
<h1>Overview</h1>
<p>Unify is built on the idea that the world is better when businesses and customers communicate freely. We exist to help streamline and enhance those conversations—with customers, prospects and enthusiasts. Our platform is packed with handy features but stays out of the way to let brands easily engage with people and build lasting relationships.</p>
<h3><a name="information-gathering-and-usage" href="#"></a>Support</h3>

<p>Any questions. Just shoot am email to support [at] tryunify.com.</p>
</article>
</section>

@stop