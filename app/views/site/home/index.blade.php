<!DOCTYPE html>
  <!--[if lte IE 9]> <html class="unify-org no-js old-ie "> <![endif]-->
  <!--[if gt IE 9]> <html class="unify-org no-js "> <!--<![endif]-->
  <!--[if !IE]> --> <html class="unify-org "> <!-- <![endif]-->
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
        <!--
        <script type="text/javascript" src="home/ruxitagentjs_2fnqr_10073150625174103.js" data-dtconfig="rid=RID_2418|rpid=1269403827|domain=ghost.org|reportUrl=/rb_orm84958|tp=500,50,0"></script><script src="https://cdn.optimizely.com/js/654983416.js"></script>
        -->
        <title>Unify provides you with the tools you need to manage your social media, understand your audience and make smarter marketing decisions.</title>
        <meta name="description" content="Unify provides you with the tools you need to manage your social media, understand your audience them and make smarter marketing decisions."/>
        <meta property="og:description" content="Unify provides you with the tools you need to manage your social media, understand your audience them and make smarter marketing decisions."/>
        <meta property="twitter:description" content="Unify provides you with the tools you need to manage your social media, understand your audience them and make smarter marketing decisions..">
        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Unify - Understand your audience">
        <meta property="twitter:site" content="@tryunify">
        <meta property="twitter:domain" content="http://tryUnify.com"/>
        <meta property="twitter:url" content="http://tryUnify.com"/>
        <meta property="og:site_name" content="Unify"/>
        <meta property="fb:admins" content="511697361"/>
        <meta property="og:title" content="Unify - understand your audience"/>
        <meta property="og:url" content="http://tryUnify.com"/>
        <meta name="HandheldFriendly" content="True"/>
        <meta name="MobileOptimized" content="320"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="cleartype" content="on"/>
        <link rel="shortcut icon" type="image/x-icon" href="assets/favicon-e13fe17b11ad125601924cfb260f20da.png"/>
        <link rel="canonical" href="index"/>
        {{ HTML::style('https://fonts.googleapis.com/css?family=Open+Sans:700,600,400,300') }}
        {{ HTML::style('https://fonts.googleapis.com/css?family=Noto+Serif') }}
        {{ HTML::style('home/assets/style.css') }}
        <link href='theme/vendors/nprogress/nprogress.css' rel='stylesheet' />


        <!-- Google Tracking Code for www.getUnify.com -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-65303751-1', 'auto');
          ga('send', 'pageview');

        </script>


        <!-- Hotjar Tracking Code for www.getUnify.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:51458,hjsv:5};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <style type="text/css">
          #Div2 {
          display: none;
          }
        </style>

            {{ HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}

    </head>

    <body class="home" data-logged-in-user=''>
        <header id="global-header" class="global-header navbar">
            <nav role="navigation">
            <div class="wrapper">
            <font size="10px" color="#4285F4"><i class="fa fa-code-fork"></i> </font><font size="10px">Unify</font>
            <ul id="main-menu">
            <li class="login-button-container"><a class="login" href="{{ URL::to('/connect') }}">Sign in</a></li>
            </ul>
            </div>
            </nav>
        </header>

    <div class="clearfix"></div>
    <main role="main">
    <section id="home-top" class="home-section home-banner full-height">
    <div class="inner">
    <h1 class="center-text">Social Analytics made simple.</h1>
    <p class="center-text">Unify is a social intelligence platform that helps you do smarter marketing. Track. Engage. Focus.</p>

        <form accept-charset="UTF-8" action="{{ URL::to('user') }}" class="clearfix home-signup-form" method="post"><div style="margin:0;padding:0;display:inline"><input type="hidden" name="_token" value="{{ csrf_token() }}"></div>
        <!--<div id="Div1"> -->
            <fieldset class="clearfix">
            <label class="">
                <input autocomplete="off" autocorrect="off" class="home-signup-input" id="name" name="name" placeholder="Your Full Name*" spellcheck="false" type="text" required="required" value="{{{ Input::old('name') }}}"/>
                <span class="error-message">Must be 1+ characters</span>
            </label>
            <label class="">
                <input autocorrect="off" class="js-trim-all-whitespace home-signup-input" id="email" name="email" placeholder="Your Email Address*" spellcheck="false" type="email" required="required" value="{{{ Input::old('email') }}}"/>
                <span class="error-message">Your email is invalid</span>
            </label>
            <label class="">
                <input autocomplete="off" autocorrect="off" class="home-signup-input" id="company" name="company" placeholder="Your Company Name" spellcheck="false" type="text" value="{{{ Input::old('company') }}}"/>
                <span class="error-message">Must be at least 7 chars</span>
            </label>
            </fieldset>
            <button class="home-signup-button button-add" type="submit" formnovalidate>Sign up free!</button>
       <!--     <button class="home-signup-button button-add" type="button" onclick="switchVisible();">Sign up free</button>
       </div>


        <div id="Div2">
            <fieldset class="clearfix">
            <label class="">
                <input autocomplete="off" autocorrect="off" class="js-trim-all-whitespace home-signup-input" id="username" name="username" placeholder="{{{ Lang::get('confide::confide.username') }}}" spellcheck="false" type="text" required="required" value="{{{ Input::old('username') }}}"/>
                <span class="error-message">Must be 1+ characters</span>
            </label>
            <label class="">
                <input autocomplete="off" autocorrect="off" class="js-trim-all-whitespace home-signup-input" id="password" name="password" placeholder="{{{ Lang::get('confide::confide.password') }}}" spellcheck="false" type="password" required="required"/>
                <span class="error-message">password</span>
            </label>
            <label class="">
                <input autocorrect="off" class="js-trim-all-whitespace home-signup-input" id="confirm_password" name="password_confirmation" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" spellcheck="false" type="password" required="required"/>
                <span class="error-message">confirm password</span>
            </label>
            
            <label class="">
            <button class="home-signup-button" type="button" onclick="switchVisible();">Previous</button>
            </label> 
            </fieldset>
            <button class="home-signup-button button-add" type="submit" formnovalidate>Sign up free!</button>
        </div> -->

        </form> 
    </div>
    <section class="home-section home-logo-quotes">
    <div class="inner">
    <div class="logo">
    <span class="quote">Track likes on</span>
    {{ HTML::image('home/assets/press-logos/wired%402x-cd74dac59994475bc8aaf2f192b7066b.png', 'Wired logo') }}
    </div>
    <div class="logo">
    <span class="quote">Track retweets on</span>
    {{ HTML::image('home/assets/press-logos/forbes%402x-1e3ea997f3fa3fa127d971c232fa2306.png', 'ProBlogger logo') }}
    </div>
    <div class="logo">
    <span class="quote">Track shares to</span>
    {{ HTML::image('home/assets/press-logos/problogger%402x-cf4c2721aa677825f9bab2af0a804f8a.png', 'Forbes logo') }}
    </div>
    <div class="logo">
    <span class="quote">Track pins on</span>
    {{ HTML::image('home/assets/press-logos/techcrunch%402x-78fecdc77e4269d3717d4bbdc7875a51.png', 'TechCrunch logo') }}
    </div>
    <div class="logo">
    <span class="quote">Track engagement on</span>
    {{ HTML::image('home/assets/press-logos/mashable%402x-93950424c286d61577f70db3c2314495.png', 'Mashable logo') }}
    </div>
    </div>
    </section>
    </section>
    <section class="home-showcase home-section left-phone">
    <div class="inner">
    <h2>Understand your audience.</h2>
    <p>Build personas with our demographic analysis, track your efforts and meet strategic goals. Our algorithms work day and night, so you can make better decisions.</p>
    <div class="device-container">
    <section class="phone screenshot-caspermobile">
    <header>
    <span class="phone-camera"><span class="hidden">Camera</span></span>
    <span class="phone-earpiece"><span class="hidden">Earpiece</span></span>
    <span class="phone-homebutton"><span class="hidden">Homebutton</span></span>
    </header>
    <section class="content">
    {{ HTML::image('home/assets/home/unify-phone3-1214fdf3539f57d5d7a0c01b0182a93b.png', 'a picture') }}
    </section>
    </section>
    <section class="browser screenshot-casperdesktop">
    <header>
    <span class="button1"><span class="hidden">Button1</span></span>
    <span class="button2"><span class="hidden">Button2</span></span>
    <span class="button3"><span class="hidden">Button3</span></span>
    <span class="fullscreen"><span class="hidden">Full Screen</span></span>
    </header>
    <section class="content">
    {{ HTML::image('home/assets/home/unify7-d593186f8aa26087df7d3847514923db.png', 'a picture') }}
    </section>
    <div class="browser-desc">
    <div class="browser-desc-left">
    <a href="#" target="_blank">{{ HTML::image('home/assets/home/square-8b7d6e075534ac051e8a60bd8c98259b.png', 'a picture') }}</a>
    <div class="browser-desc-industry"><em>Reach your audience.</em><br>Anywhere in the world.</div>
    </div>
    <p class="browser-desc-caption">Focus on the right audience and skyrocket your ROI. What could you do with extra revenue?</p>
    </div>
    </section>
    </div>
    </div>
    </section>
    <section class="home-section right-phone">
    <div class="inner">
    <h2>Present your business in the best light possible.</h2>
    <p>Manage all your social media sharing from one place. Stay up to date with your community, Make more connections and skyrocket your growth!</p>
    <div class="device-container">
    <section class="phone screenshot-caspermobile">
    <header>
    <span class="phone-camera"><span class="hidden">Camera</span></span>
    <span class="phone-earpiece"><span class="hidden">Earpiece</span></span>
    <span class="phone-homebutton"><span class="hidden">Homebutton</span></span>
    </header>
    <section class="content">
    {{ HTML::image('home/assets/home/unify-phone10-ea637ca4bd0f820828e5a62736ca314f.png', 'a picture') }}
    </section>
    </section>
    <section class="browser screenshot-casperdesktop">
    <header>
    <span class="button1"><span class="hidden">Button1</span></span>
    <span class="button2"><span class="hidden">Button2</span></span>
    <span class="button3"><span class="hidden">Button3</span></span>
    <span class="fullscreen"><span class="hidden">Full Screen</span></span>
    </header>
    <section class="content">
    {{ HTML::image('home/assets/home/unify8-b679707413b256ed2b1d1c670dd2aa34.png', 'a picture') }}
    </section>
    </section>
    </div>
    </div>
    </section>
    <section class="home-section left-phone">
    <div class="inner">
    <h2>Track ROI at a glance.</h2>
    <p>You make the connections, we track what's working. Every mention, impression, like, share, follow, pin, post and reach will be ready for your reports and planning.</p>
    <div class="device-container">
    <section class="phone screenshot-caspermobile">
    <header>
    <span class="phone-camera"><span class="hidden">Camera</span></span>
    <span class="phone-earpiece"><span class="hidden">Earpiece</span></span>
    <span class="phone-homebutton"><span class="hidden">Homebutton</span></span>
    </header>
    <section class="content">
    {{ HTML::image('home/assets/home/square-phone1-9d6e89d8458e8e91902429991c5f9b06.jpg', 'a picture') }}
    </section>
    </section>
    <section class="browser screenshot-casperdesktop">
    <header>
    <span class="button1"><span class="hidden">Button1</span></span>
    <span class="button2"><span class="hidden">Button2</span></span>
    <span class="button3"><span class="hidden">Button3</span></span>
    <span class="fullscreen"><span class="hidden">Full Screen</span></span>
    </header>
    <section class="content">
    {{ HTML::image('home/assets/home/square1-ef96b2a6539b8ee4602fa50ae4f5b22f.png', 'a picture') }}
    </section>
    </section>
    </div>
    </div>
    </section>
    <!--
    <section class="home-section right-phone">
    <div class="inner">
    <h2>Present your business in the best light possible</h2>
    <p>Work smarter with online invoicing software. Create professional recurring invoices, schedule bill payments to manage cashflow and invoice groups of customers – with one click. Payments, returns and credits are all tracked automatically.</p>
    <div class="device-container">
    <section class="phone screenshot-caspermobile">
    <header>
    <span class="phone-camera"><span class="hidden">Camera</span></span>
    <span class="phone-earpiece"><span class="hidden">Earpiece</span></span>
    <span class="phone-homebutton"><span class="hidden">Homebutton</span></span>
    </header>
    <section class="content">
    <img alt="Ghost phone1" src="assets/home/ghost-phone1-f40ca2311dbadfbfdd44c8eb7fc25914.png"/>
    </section>
    </section>
    <section class="browser screenshot-casperdesktop">
    <header>
    <span class="button1"><span class="hidden">Button1</span></span>
    <span class="button2"><span class="hidden">Button2</span></span>
    <span class="button3"><span class="hidden">Button3</span></span>
    <span class="fullscreen"><span class="hidden">Full Screen</span></span>
    </header>
    <section class="content">
    <img alt="Ghost9" src="assets/home/ghost9-c23bf2d596e0139ecfd1d3e7052a86f5.png"/>
    </section>
    </section>
    </div>
    </div>
    </section> -->
    <section class="home-section home-sign-up-banner">
    <div class="inner">
    <a class="button-add large" data-arcticscroll="true" data-speed="600" href="#home-top">Get early access to Unify for free!</a>
    </div>
    </section>
    <div class="home-section users-showcase">
    <img alt="Users" src="home/assets/home/users-0cbc7b843355f4eb4e0d760057b4573c.png"/>
    <div class="inner">
    <h2 class="subtitle">Join some of the world's most important organisations who make better marketing decisions with Unify</h2>
    </div>
    </div>
    <br> <br>
    </main>
    <div class="clearfix"></div>
    <footer id="global-footer">
    <nav id="footer-nav" role="navigation">
    <ul id="footer-menu">
    <!--<li class="logo"><a href="#"><img alt="Unify" src="assets/logo.png"/></a></li>-->
    <li><a class="" href="{{ URL::to('/about') }}">About</a></li>
    <li><a class="" href="{{ URL::to('/pricing') }}">Pricing</a></li>
    <li><a class="" href="{{ URL::to('/terms') }}">Terms</a></li>
    <li><a class="" href="{{ URL::to('/privacy') }}">Privacy</a></li>
    <li><a class="" href="mailto:support@tryunify.com?Subject=Hello%20again" target="_top">Contact</a></li>
    </ul>
    <div class="clearfix"></div>
    <span class="poweredby">{{ HTML::image('home/assets/logo.png', 'a picture') }} All rights reserved. <em>© 2015</em> <a href="http://tryunify.com" target="_blank">Unify</a></span>
    <ul id="social-menu">
    <li class="github"><a href="#"><span class="hidden">Github</span></a></li>
    <li class="twitter"><a href="https://twitter.com/charge_loop"><span class="hidden">Twitter</span></a></li>
    <li class="gplus"><a href="#"><span class="hidden">Google+</span></a></li>
    <li class="facebook"><a href="#"><span class="hidden">Facebook</span></a></li>
    </ul>
    </nav>
    <div class="clearfix"></div>
    </footer>

    <!-- javascript -->
     {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js') }}
     <!--[if lte IE 9]>
     {{ HTML::script('home/assets/vendor/flexie.min.js') }}
     <![endif]-->
     {{ HTML::script('home/assets/application-f70a08abb1fd9fbfdd4ae25ddd2c3d6b.js') }}

        <script src='theme/vendors/nprogress/nprogress.js'></script>
        <script type="text/javascript">
    function switchVisible() {
                if (document.getElementById('Div1')) {

                    if (document.getElementById('Div1').style.display == 'none') {
                        document.getElementById('Div1').style.display = 'block';
                        document.getElementById('Div2').style.display = 'none';
                    }
                    else {
                        document.getElementById('Div1').style.display = 'none';
                        document.getElementById('Div2').style.display = 'block';
                    }
                }
    }

    $('body').show();
    $('.version').text(NProgress.version);
    NProgress.start();
    setTimeout(function() { NProgress.done(); $('.fade').removeClass('out'); }, 1000);

      //  $('#email').keyup(function(){
      //  $('#username').val(this.value);
      //  });

    </script>

    </body>

</html>
