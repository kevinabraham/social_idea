@extends('site.home.layout')  
@section('content')

<div class="clearfix"></div>
<main role="main">
<section class="page-wrapper">
<nav class="page-nav">
<ul>
<li class=''><a class="" href="{{ URL::to('/about') }}">Overview</a></li>
<li class='current current-parent'><a class="" href="{{ URL::to('/guidelines') }}">Guidelines</a></li>
<li class=''><a class="" href="{{ URL::to('/trademark') }}">Trademark Policy</a></li>
<li class=''><a class="" href="{{ URL::to('/privacy') }}">Privacy Policy</a></li>
<li class=''><a class="" href="{{ URL::to('/terms') }}">Terms of Service</a></li>
<li class=''><a class="" href="mailto:support@getchargeloop.com?Subject=Hello%20again" target="_top">Contact</a></li>
</ul>
</nav>
<article class="page-content">
<h1>Community Guidelines</h1>
<p>Our Community Guidelines protect the communities which exist around the ChargeLoop software including, among other things, our forum, IRC channel and GitHub repositories. They are designed to make it clear that we believe in tolerance, respect, inclusion and hard work. The ChargeLoop community consists of an incredible group of people all over the world, and we want to continue to see it flourish.</p>
<p>The internet is full of codes of conduct, employee guidelines, community charters and then some. We’ve custom-written some bits of ours, but for a lot of this, borrowed from people, companies and projects that have already done solid jobs at this very task (see our reference list below). No part of this is an endorsement of any of these people, companies or projects—simply us acknowledging that the specific language we’re referencing seems to mirror our own ideals and intentions.</p>
<h3><a name="community-goals" href="#community-goals"></a>General Values</h3>
<p>We always try to...</p>
<ol>
<li><strong>Be open</strong>: We invite anybody, from any company or from no company, to participate in any aspect of our projects. Our community is open, and any responsibility can be carried by any contributor who demonstrates the required capacity and competence.</li>
<li><strong>Be positive</strong>: There is so much more value in building people up than breaking them down. Even when we have to give an unpopular answer, we try to do so in a friendly and positive manner.</li>
<li><strong>Be respectful</strong>: We work together to resolve conflict, placing facts before opinions. Heated debate is valuable and pushes everyone consider different points of view, but we don’t allow frustration to turn into a personal attack.</li>
<li><strong>Be collaborative</strong>: Collaboration reduces redundancy and improves the quality of our work. We prefer to work transparently and involve interested parties as early as possible.</li>
<li><strong>Be pragmatic</strong>: Nobody knows everything! Asking questions early avoids problems later, so questions are encouraged, though they may be directed to the appropriate forum. Those who are asked should be responsive and helpful.</li>
<li><strong>Step down considerately</strong>: Members of every project come and go. When somebody leaves or disengages from the project or community, we ask that they do so in a way that minimizes disruption to the rest. They should tell people they are leaving and take the proper steps to ensure that others can pick up where they left off.</li>
<li><strong>Take responsibility</strong>: We can all make mistakes; when we do, we take responsibility for them. If someone has been harmed or offended, we listen carefully and respectfully, and work to right the wrong.</li>
</ol>
<h3><a name="community-dbad" href="#community-dbad"></a>Don't be a Jerk</h3>
<p>You shall be judged to have <a href="http://meta.wikimedia.org/wiki/Don%27t_be_a_dick">been a jerk</a> when a group of your peers have deemed that you were being a jerk. If, after being warned, you continue to be a jerk: you will be banned. There are no exceptions to this rule. Common examples of dickery include (but are not limited to):</p>
<ol>
<li>Sexist, heterosexist, racist, or otherwise hateful remarks. Which are not tolerated under <strong>any circumstances</strong>. Swearing is fine, but never when directed at people.</li>
<li>Don't harass or bully people. Do not attempt to communicate with someone who has asked you to stop.</li>
<li>Don’t spam. Don’t make spammy posts, don’t post spammy replies, don’t send people spammy messages. Nobody likes spam. Nobody.</li>
<li>Don't impersonate or do things that would cause confusion between you and a person or company.</li>
<li>Don't do anything illegal. Don't screw with trademarks, copyrights, fraud or anything else that the police would not be happy to see you doing.</li>
</ol>
<h3><a name="community-coi" href="#community-coi"></a>Conflicts of Interest</h3>
<p>We expect leaders and community members to be aware when they are conflicted due to employment or other projects they are involved in, and abstain from or delegate decisions that may be perceived as self-interested.</p>
<p>When in doubt, ask for a second opinion. Perceived conflicts of interest are important to address; as a leader, act to ensure that decisions are credible even if they must occasionally be unpopular, difficult or favorable to the interests of one group over another.</p>
<h3><a name="community-summary" href="#community-summary"></a>Summary</h3>
<p>These guidelines are not exhaustive or complete, nor are they a rulebook. They serve to express our common goals and sentiments for our community. We expect them to be followed in spirit as much as in the letter. We borrowed liberally from other projects with excellent guidelines:</p>
<ul>
<li><a href="http://emberjs.com/guidelines/">Ember.js Community Guidelines</a></li>
<li><a href="http://wiki.mozilla.org/Code_of_Conduct/Draft">Mozilla Code of Conduct</a></li>
<li><a href="http://github.com/npm/policies/blob/master/conduct.md">Npm Code of Conduct</a></li>
<li><a href="http://www.tumblr.com/policy/en/community">Tumblr Community Guidelines</a></li>
</ul>
<h3><a name="community-report" href="#community-report"></a>Reporting Violations</h3>
<p>If you feel that a member (or members) of the ChargeLoop community is violating these guidelines, please report it immediately to support@getchargeloop.com</script></p>
</article>
</section>

@stop