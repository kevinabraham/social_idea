@extends('site.layouts.basic')

@section('head')
        <style type="text/css">
            .bs-item {
                background: #fff;
                margin-bottom: 30px;
                height: 100px;
                text-align: center;
                padding: 10px;
                font-size: 14px;
                border-radius: 2px;
            }

            .btn-block {
                height: 100%;
            }

        </style>
@stop
        

    
        
       
    @section('content')    
        
            <section id="content">
                <div class="container">
                    
                    
                    
                    
                    <!-- Bottom Only -->
                    <div class="m-b-30">
                        <div class="block-header">
                            <h2>Connect your social profiles</h2>
                        </div>
                    
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">
                                <div class="bs-item z-depth-2-bottom">

                                    <button class="btn btn-default btn-block bgm-cyan btn-icon-text" onclick="location.href = '{{{ URL::to('twitter/login') }}}';"><font size="5"> twitter </font></button>
                                </div>
                            </div>
                            
                            <div class="col-sm-4 col-xs-6">
                                <div class="bs-item z-depth-2-bottom">
                                    <button class="btn btn-default btn-block bgm-indigo btn-icon-text" onclick="location.href = '{{{ URL::to('/sign-in-with-facebook') }}}';"><font size="5"> Facebook </font></button>
                                </div>
                            </div>
                            
                            <div class="col-sm-4 col-xs-6">
                                <div class="bs-item z-depth-2-bottom">
                                    <button class="btn btn-default btn-block bgm-red btn-icon-text"><font size="5"> Youtube </font></button>
                                </div>
                            </div>
                            
                            <div class="col-sm-4 col-xs-6">
                                <div class="bs-item z-depth-2-bottom">

                                    <button class="btn btn-default btn-block bgm-pink btn-icon-text"><font size="5"> Pinterest </font></button>
                                </div>
                            </div>
                            
                            <div class="col-sm-4 col-xs-6">
                                <div class="bs-item z-depth-2-bottom">
                                    <button class="btn btn-default btn-block bgm-green btn-icon-text"><font size="5"> Google</font></button>
                                </div>
                            </div>
                            
                            <div class="col-sm-4 col-xs-6">
                                <div class="bs-item z-depth-2-bottom">
                                    <button class="btn btn-default btn-block bgm-bluegray btn-icon-text"><font size="5"> LinkedIn</font></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@stop