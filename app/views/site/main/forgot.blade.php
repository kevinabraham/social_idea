 @extends('site.layouts.basic')  
 
        @section('content')

                   <section id="content">
                <div class="container">
                    <div class="block-header">

                    <div class="card z-depth-3-bottom">
                        
                        <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('/users/forgot_password') }}" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                            <div class="card-header bgm-indigo">
                                <h2>Login</h2>
                            </div>
                            
                            <div class="card-body card-padding">
                            @if (Session::get('error'))
                                <div class="alert alert-danger" role="alert">{{{ Session::get('error') }}}</div>
                            @endif

                            @if (Session::get('notice'))
                                <div class="alert alert-info" role="alert">{{{ Session::get('notice') }}}</div>
                            @endif
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" name="email" id="email" value="{{{ Input::old('email') }}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <button class="btn bgm-indigo btn-icon-text btn-lg btn-block waves-effect"><i class="md md-done-all"></i> {{{ Lang::get('confide::confide.forgot.submit') }}}</button>
                                    </div>
                                </div>
                        </form>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-xs-8">

                                                <p class="text-left">Dont have an account yet? Sign up today!</p>

                                        </div>
                                    </div>
                                  
                                </div>

                            </div>
                        </form>
                    </div>
                    

                </div>
            </section>

@stop