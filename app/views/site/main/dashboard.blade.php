@extends('site.layouts.master')  
  @section('head')
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  @stop  
        @section('content')

              <section id="content">
                    <div class="container">
                        <div class="block-header">
                            <h2>Your connected social profile's</h2>
                        </div>
                    
                        <div class="card m-b-0" id="messages-main">
                           
                            
                            <div class="ms-menu">
                                <div class="ms-block">
                                    <div class="ms-user">
                                        <img src="{{ $profile_picture }}" alt="">
                                        <div>Signed in as <br/> {{ $name }}</div>
                                    </div>
                                </div>
                                
                                <div class="ms-block">
                                    <div class="dropdown" data-animation="flipInX,flipOutX">
                                        <a class="btn btn-primary btn-block" href="#" data-toggle="dropdown">Add account <i class="caret m-l-5"></i></a>

                                        <ul class="dropdown-menu dm-icon w-100">
                                            <li><a href="{{{ URL::to('twitter/login') }}}"><i class="fa fa-twitter-square"></i>&nbsp&nbsp&nbspTwitter</a></li>
                                            <li><a href="{{{ URL::to('/sign-in-with-facebook') }}}"><i class="fa fa-facebook-square"></i>&nbsp&nbsp&nbspFacebook</a></li>
                                            <li><a href="#"><i class="fa fa-google-plus-square"></i>&nbsp&nbsp&nbspGoogle</a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="listview lv-user m-t-20">
                                                                  
                                    @foreach ($social_profile as $social_profile)

                                    <form name="social_media_id" type="hidden" action="{{ URL::to('/social_media_id') }}" id="social_media_id">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="social_media_id" id="social_media_id" value="{{ $social_profile->social_media_id }}" />
                                            <input type="hidden" name="social_media_type" id="social_media_type" value="{{ $social_profile->social_media_type }}" />

                                    
                                    <div class="lv-item media navbar-default">
                                    <button type="submit" class="social_media_id">
                                        <div class="lv-avatar bgm-blue pull-left"><i class="fa fa-{{ $social_profile->social_media_type }}"></i></div>
                                        <div class="media-body">
                                            <div class="lv-title">{{ '@' . $social_profile->screen_name }}</div>
                                            <div class="lv-small">{{ $social_profile->social_media_type }}</div>
                                            
                                            
                                            
                                        </div>
                                        </button>
                                    </div>
                                    
                                    </form>
                                    @endforeach
                                    
                                    
                                    
                                    
                                    
                                </div>

                                
                            </div>
                            
                            <div class="ms-body">
                                <div class="listview lv-message">
                                    <div class="lv-header-alt bgm-white">
                                        <div id="ms-menu-trigger">
                                            <div class="line-wrap">
                                                <div class="line top"></div>
                                                <div class="line center"></div>
                                                <div class="line bottom"></div>
                                            </div>
                                        </div>


                                        <div class="lvh-label hidden-xs">


                                        
                                            <div class="lv-avatar pull-left">
                                                <img src="{{ $profile_picture }}" alt="">
                                            </div>
                                            <span class="c-black">{{ '@' . $screen_name }}</span>
                                        </div>
                                        
                                        <ul class="lv-actions actions">
                                            <li>
                                                <a href="#">
                                                    <i class="md md-delete"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="md md-check"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="md md-access-time"></i>
                                                </a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="md md-sort"></i>
                                                </a>
                                    
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="#">Latest</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Oldest</a>
                                                    </li>
                                                </ul>
                                            </li>                             
                                            <li class="dropdown">
                                                <a href="#" data-toggle="dropdown" aria-expanded="true">
                                                    <i class="md md-more-vert"></i>
                                                </a>
                                    
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li>
                                                        <a href="#">Refresh</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Message Settings</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                    <div class="lv-body"> 


                    <div class="card">
                            
                            <div class="card-body card-padding">
                                <div role="tabpanel">
                                    <ul class="tab-nav" role="tablist">
                                        <li class="active"><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab">TimeLine</a></li>
                                        <li><a href="#profile11" aria-controls="profile11" role="tab" data-toggle="tab">Post</a></li>
                                        <li><a href="#messages11" aria-controls="messages11" role="tab" data-toggle="tab">Messages</a></li>
                                        <li><a href="#settings11" aria-controls="settings11" role="tab" data-toggle="tab">Analytics</a></li>
                                    </ul>
                                  
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home11">

                                        
                                    <div class="media-demo">
                                    @foreach ($tweet as $tweet)
                                    <div class="media">
                                        <div class="pull-left">
                                            <a href="#">
                                                <img class="media-object" src="{{ $tweet->user->profile_image_url }}" alt="profile picture">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="http:'{{ Twitter::linkUser($tweet->user) }}'">{{ $tweet->user->name }}</a></h4>
                                            {{ Twitter::linkify($tweet->text) }}
                                            <ul class="lv-attrs">
                                                <li>Posted: {{ Twitter::ago($tweet->created_at) }}</li>
                                                <li>Retweets: {{ Twitter::linkify($tweet->retweet_count) }}</li>
                                                <li>Favourites: {{ Twitter::linkify($tweet->favorite_count) }}</li>
                                                <li>Reach: {{ Twitter::linkify($tweet->user->friends_count) }}</li>
                                            </ul>
                                        </div>

                                    </div>

                                    @endforeach 
                                   


                                    
                                </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="profile11">
                                          <div class="form-group">
                                            <div class="fg-line">
                                                <form name="twitter_post" type="hidden" action="{{ URL::to('/twitter/post_tweet') }}" id="twitter_post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="social_media_id" id="social_media_id" value="{{ $social_media_id }}" />
                                                <input type="hidden" name="screen_name" id="screen_name" value="{{ $screen_name }}" />
                                                <input type="hidden" name="x_auth_expires" id="x_auth_expires" value="{{ $x_auth_expires }}" />
                                                <input type="hidden" name="oauth_token" id="oauth_token" value="{{ $oauth_token }}" />
                                                <input type="hidden" name="oauth_secret" id="new_oauth_secret" value="{{ $oauth_secret }}" />
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="md  md-mode-comment"></i></span>
                                                    <div class="fg-line">
                                                        <input type="text" class="form-control input-lg auto-size" placeholder="What would you like to post?" name="tweet_text" id="tweet_text">
                                                    </div>
                                                </div>
                                                </div>

                                          </div>
                                            
                                              <div class="col-sm-6">
                                                <button class="btn btn-default btn-icon waves-effect waves-circle waves-float" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="md md-alarm"></i></button>
                                                &nbsp
                                                <button class="btn btn-default btn-icon waves-effect waves-circle waves-float" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2"><i class="md md-attach-file"></i></button>
                                                &nbsp
                                                <button class="btn btn-default btn-icon waves-effect waves-circle waves-float" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3"><i class="md md-gps-fixed"></i></button>
                                              </div>

                                              <div class="col-sm-6">
                                                <button class="btn btn-success btn-lg hec-save waves-effect btn-icon-text pull-right"><i class="md md-send"></i>Post</button>
                                              </div>
                                    
<br>
<br>
<br>
                                            <div class="collapse m-t-10" id="collapseExample">
                                            <p class="f-500 c-black m-b-15">Schedule your posts</p>
                                                <div class="input-group form-group">
                                                        <span class="input-group-addon"><i class="md md-event"></i></span>
                                                            <div class="dtp-container dropdown fg-line">
                                                            <input type='text' class="form-control date-time-picker" data-toggle="dropdown" placeholder="Choose when your post will be sent">
                                                        </div>
                                                    </div>
                                            </div>
                                            
                                            <div class="collapse m-t-10" id="collapseExample2">
                                            <p class="f-500 c-black m-b-15">Upload an image</p>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput"></div>
                                                    <div>
                                                        <span class="btn btn-info btn-file">
                                                            <span class="fileinput-new">Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="file" name="...">
                                                        </span>
                                                        <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </div>   

                                            <div class="collapse m-t-10" id="collapseExample3">
                                            <p class="f-500 c-black m-b-15">Set a location</p>
                                                <select class="tag-select" data-placeholder="Choose a Country...">
                                                    <option value="United States">United States</option>
                                                    <option value="United Kingdom">United Kingdom</option>
                                                    <option value="Afghanistan">Afghanistan</option>
                                                    <option value="Aland Islands">Aland Islands</option>
                                                    <option value="Albania">Albania</option>
                                                    <option value="Algeria">Algeria</option>
                                                    <option value="American Samoa">American Samoa</option>
                                                </select>
                                            </div> 

                                            
                                         

                                          </form>
                                        </div>



                                        <div role="tabpanel" class="tab-pane" id="messages11">
                                           <p>Praesent turpis. Phasellus magna. Fusce vulputate eleifend sapien. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis.</p>
                                        </div>  
                                        
                                        <div role="tabpanel" class="tab-pane" id="settings11">
                                            <p>Praesent turpis. Phasellus magna. Fusce vulputate eleifend sapien. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis.</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <br/>




                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>


    @stop  


@section('javascript')

    <script type="text/javascript">
            $(document).ready(function(){
                //Welcome Message (not for login page)
                function notify(message, type){
                    $.growl({
                        message: message
                    },{
                        type: type,
                        allow_dismiss: false,
                        label: 'Cancel',
                        className: 'btn-xs btn-inverse',
                        placement: {
                            from: 'top',
                            align: 'right'
                        },
                        delay: 2500,
                        animate: {
                                enter: 'animated fadeIn',
                                exit: 'animated fadeOut'
                        },
                        offset: {
                            x: 20,
                            y: 85
                        }
                    });
                };
                
                if (!$('.login-content')[0]) {
                    notify('Welcome back {{ $name }}', 'inverse');
                } 
                });
        </script>

@stop      