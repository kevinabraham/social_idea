 @extends('site.layouts.basic')  
 
        @section('content')

                   <section id="content">
                <div class="container">
                    <div class="block-header">

                    <div class="card z-depth-3-bottom">
                        
                        <form class="form-horizontal" role="form">
                            <div class="card-header bgm-indigo">
                                <h2>Login</h2>
                            </div>
                            
                            <div class="card-body card-padding">
                            <form role="form" method="POST" action="{{{ URL::to('user/login') }}}" accept-charset="UTF-8" id="login_form">
       							 <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
							<!-- error handling -->
				             @if (Session::get('error'))
				                <div class="alert alert-error" role="alert">{{{ Session::get('error') }}}</div>
				             @endif
				             
				             @if (Session::get('error'))
				                <div class="alert alert-info" role="alert">{{{ Session::get('error') }}}</div>
				             @endif
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                                    @if (Cache::remember('username_in_confide', 5, function() {
                return Schema::hasColumn(Config::get('auth.table'), 'username');
            }))
                                            <input type="text" class="form-control input-sm" id="inputEmail3" placeholder="{{{ Lang::get('confide::confide.username_e_mail') }}}" type="text" name="email" value="{{{ Input::old('email') }}}">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-8">
                                        
                                        <div class="fg-line">
                                            <input type="password" class="form-control input-sm" id="inputPassword3" placeholder="{{{ Lang::get('confide::confide.password') }}}" name="password" id="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-xs-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" id="remember" value="1">
                                                <i class="input-helper"></i>
                                                {{{ Lang::get('confide::confide.login.remember') }}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">

                                       <p class="text-right"><a href="{{{ URL::to('/users/forgot_password') }}}">{{{ Lang::get('confide::confide.login.forgot_password') }}}</a></p>

                                </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <button type="submit" class="btn bgm-indigo btn-icon-text btn-lg btn-block waves-effect"><i class="md md-done-all"></i> Login</button>
                                    </div>
                              
                                </div>
</form>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-xs-8">

                                                <p class="text-left">Dont have an account yet? Sign up today!</p>

                                        </div>
                                    </div>
                                  
                                </div>

                            </div>
                        </form>
                    </div>
                    

                </div>
            </section>

@stop