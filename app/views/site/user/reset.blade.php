 @extends('site.layouts.basic')  
 
        @section('content')

                   <section id="content">
                <div class="container">
                    <div class="block-header">

                    <div class="card z-depth-3-bottom">
                     
                            <div class="card-header bgm-indigo">
                                <h2>Forgot password</h2>
                            </div>
                            
                            <div class="card-body card-padding">                           
                                {{ Confide::makeResetPasswordForm($token)->render() }}
                                <p class="text-left"><a href="{{ URL::to("user/forgot") }}">Back</a></p>
                                  
                                </div>

                            </div>
                        </form>
                    </div>
                    

                </div>
            </section>

@stop