 @extends('site.layouts.basic')  
 
        @section('content')

                   <section id="content">
                <div class="container">
                    <div class="block-header">

                    <div class="card z-depth-3-bottom">
                        
                        <form class="form-horizontal" role="form" method="POST" action="{{ URL::to('user') }}" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                            <div class="card-header bgm-indigo">
                                <h2>Sign up</h2>
                            </div>
                            <!-- error handling -->
                            <div class="card-body card-padding">
                            @if (Session::get('error'))
                                 <div class="alert alert-danger" role="alert">
                            @if (is_array(Session::get('error')))
                                {{ head(Session::get('error')) }}
                            @endif
                                </div>
                            @endif

                            @if (Session::get('notice'))
                                <div class="alert alert-info" role="alert">{{ Session::get('notice') }}</div>
                            @endif
                            <!-- end error handling -->
                                <div class="form-group">
                                    <label for="inputName3" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" id="inputName3" placeholder="Your full name" name="name" value="{{{ Input::old('name') }}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">{{{ Lang::get('confide::confide.e_mail') }}}</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="email" class="form-control input-sm" id="inputEmail3" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" name="email" value="{{{ Input::old('email') }}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputCompany3" class="col-sm-2 control-label">Company</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" id="inputCompany3" placeholder="Your company name" name="company" value="{{{ Input::old('company') }}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">{{{ Lang::get('confide::confide.username') }}}</label>
                                    <div class="col-sm-8">
                                        <div class="fg-line">
                                            <input type="text" class="form-control input-sm" id="inputUsername3" placeholder="{{{ Lang::get('confide::confide.username') }}}" name="username" value="{{{ Input::old('username') }}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">{{{ Lang::get('confide::confide.password') }}}</label>
                                    <div class="col-sm-8">
                                        
                                        <div class="fg-line">
                                            <input type="password" class="form-control input-sm" id="inputPassword3" placeholder="{{{ Lang::get('confide::confide.password') }}}" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
                                    <div class="col-sm-8">
                                        
                                        <div class="fg-line">
                                            <input type="password" class="form-control input-sm" id="inputPasswordConfirmation3" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <button class="btn bgm-indigo btn-icon-text btn-lg btn-block waves-effect"><i class="md md-done-all"></i> Sign up</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-xs-8">

                                                <p class="text-left"><a href="{{ URL::to("user/login") }}">Already have an account? Sign in.</a></p>

                                        </div>
                                    </div>
                                  
                                </div>

                            </div>
                        </form>
                    </div>

            </section>

@stop