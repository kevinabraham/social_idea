@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.login') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
	<h1>Login into your account</h1>
</div>
{{ Confide::makeLoginForm()->render() }}

 <a class="btn btn-twitter btn-block" href="{{{ URL::to('/sign-in-with-twitter') }}}"><i class=
                       "fa fa-twitter pull-left"></i>Sign in with Twitter</a>

 <a class="btn btn-reddit btn-block" href="{{{ URL::to('/sign-in-with-facebook') }}}"><i class=
                       "fa fa-facebook pull-left"></i>Sign in with facebook</a>
@stop
