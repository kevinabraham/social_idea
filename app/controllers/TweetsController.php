<?php

class TweetsController extends \BaseController {

	/**
	 * Display a listing of tweets
	 *
	 * @return Response
	 */
	public function index()
	{
		$tweets = Tweet::all();

		return View::make('tweets.index', compact('tweets'));
	}

	/**
	 * Show the form for creating a new tweet
	 *
	 * @return Response
	 */
	public function create()
	{
		$tags = Tag::lists('tag', 'id');
		return View::make('tweets.create', compact('tags'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		 $tweet = array(
			                
			                'body'      => Input::get('body')
			            );

			              DB::table('tweets')->insert( $tweet );

			              return Twitter::postTweet(array('status' => $tweet, 'format' => 'json'));
		
	}


	/**
	 * Send a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function send()
	{
		 $tweet = array(
			                
			                'body'      => Input::get('body')
			            );

			              DB::table('tweets')->insert( $tweet );

			              return Twitter::postTweet(array('status' => 'This is a test tweet sent directly through twitter API', 'format' => 'json'));
		
	}

	/**
	 * Display the specified tweet.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tweet = Tweet::findOrFail($id);

		return View::make('tweets.show', compact('tweet'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tweet = $this->tweet->find($id);
		$tags = Tag::lists('tag', 'id');
		$tweetTags = $tweet->tags()->get();

		$selectedTags = array();
		foreach ($tweetTags as $tag) {
			$selectedTags[] = $tag->id;
		}

		if (is_null($tweet))
		{
			return Redirect::route('tweets.index');
		}

		return View::make('tweets.edit', compact('tweet', 'tags', 'selectedTags'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), array('_method', 'tags'));
		$validation = Validator::make($input, Tweet::$rules);

		if ($validation->passes())
		{
			$tweet = $this->tweet->find($id);

			$tweet->update($input);
			$tags = Input::get('tags');

			$tweet->tags()->sync($tags);

			return Redirect::route('tweets.show', $id);
		}

		return Redirect::route('tweets.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified tweet from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Tweet::destroy($id);

		return Redirect::route('tweets.index');
	}

}
