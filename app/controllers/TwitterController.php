<?php 

class TwitterController extends BaseController {

	// this is for public functions. but I think I probably wont use this and use models instead. 
    /**
     * Twitter Model
     * @var Twitter
     */
    protected $twitter;

     /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user, Twitter $twitter)
    {
        parent::__construct();
        $this->user = $user;
    }


	// function to return dashboard with first found profile data about the logged in user
	 public function dashboard()
	{
			return View::make('site.main.dashboard', 
			[
				'tweet' 			   => Social::getTweets(), 
				'username'             => Auth::user()->username, 
				'name'                 => Social::getTwitterName(),  
				'profile_picture'      => Social::getTwitterProfilePicture(), 
				'screen_name'          => Social::getTwitterScreenName(),
				'social_profile'       => Social::getSocialProfile(),  
				'main_profile_picture' => Social::getAnyProfilePicture(), 
				'social_media_id'      => Social::getSocialMediaID(),  
				'social_media_type'    => Social::getSocialMediaType(),  
				'oauth_token'          => Social::getOauthToken(), 
				'oauth_secret'         => Social::getOauthSecret(),  
				'x_auth_expires'       => Social::getXauthExpires()  								 
		    ]);

	}	


	// function to return a clicked on profile.
	public function uniqueProfile()
	 { 

			$social_media_id = Input::get('social_media_id');
			$social_media_type = Input::get('social_media_type');
			$username = Auth::user()->username;
            $name = DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('name');
  			$profile_picture = DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('profile_picture');
  			$social_profile = DB::table('profiles')->where('user_id', Auth::user()->id)->get();
			$screen_name = DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('screen_name');
			$main_profile_picture = DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('profile_picture');
			$oauth_token = DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('oauth_token');
			$oauth_secret = DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('oauth_secret');
			$x_auth_expires = DB::table('profiles')->where('social_media_id', $social_media_id)->pluck('x_auth_expires');
			//$dm = Twitter::getDmsIn(array('count' => 10, 'format' => 'object'));


			return View::make('site.main.dashboard', 
			[
				'tweet'                => Social::getTweets(), 
				'username'             => $username, 
				'name'                 => $name, 
				'profile_picture'      => $profile_picture, 
				'screen_name'          => $screen_name, 
				'social_profile'       => $social_profile, 
				'main_profile_picture' => $main_profile_picture, 
				'social_media_id'      => $social_media_id, 
				'social_media_type'    => $social_media_type, 
				'oauth_token'          => $oauth_token, 
				'oauth_secret'         => $oauth_secret, 
				'x_auth_expires'       => $x_auth_expires 
			]);	
		
	 }	

	 // function to post a tweet from the dashboard
	 public function postTweet()
	 {

	 	$new_token = array(
			                    'oauth_token'        => Input::get( 'oauth_token' ),
			                    'oauth_token_secret' => Input::get( 'oauth_secret' ),
			                    'user_id'            => Input::get( 'social_media_id' ),
			                    'screen_name'        => Input::get( 'screen_name' ),
			                    'x_auth_expires'     => Input::get( 'x_zauth_expires' )
        );

	 	$tweet_text = Input::get( 'tweet_text' );
		$social_post = array(
						        //'social_media_id'    => Input::get( 'social_media_id' ),
						        'tweet_text'         => Input::get( 'scheduled_time' ),
						        'scheduled_time'     => Input::get( 'scheduled_time' ),
						        'created_at'         => new DateTime,
						        'updated_at'         => new DateTime,
        );

        Session::forget('access_token');
        Session::put('access_token', $new_token);
       
       if($social_post->scheduled_time == !null)
       {
        	Twitter::postTweet(['status' => $tweet_text, 'format' => 'json']);
       }
       else 
       {

       } 

    	return Redirect::route('dashboard');
    	
	 }

	
	// test function to return timeline data for the user
	public function tweetData()
	{   
		    $tweets         = Twitter::getUserTimeline(array('screen_name' => 'imkevinabraham', 'count' => 20, 'format' => 'object'));
		    $screen_name    = DB::table('profiles')->where('user_id', Auth::user()->id)->pluck('screen_name');
		    $social_profile = DB::table('profiles')->where('user_id', Auth::user()->id)->get();
		 
		    return View::make('testing.test', ['tweet' => $tweet, 'screen_name' => $screen_name, 'social_profile' => $social_profile]);

	}

	// function to return the post that the user wants to post on twitter
	public function getTweet()
	{
	/*	$social_media_id = Input::get('social_media_id');
		$social_media_type = Input::get('social_media_type');
		$unique_profile = DB::table('profiles')->where('social_media_id', $social_media_id)->first();
		$tweet_text = Input::get('tweet_text');
		$screen_name = $unique_profile->screen_name;
		$token = Input::get( 'oauth_token' );
	    $verify = Input::get( 'oauth_secret' );
		

		dd(Twitter::postTweet(array('screen_name' => $screen_name, 'id' => $social_media_id, 'status' => $tweet_text, 'format' => 'json')));
  */
	
	//dd(Twitter::postTweet(['status' => 'Laravel is beautiful', 'format' => 'json']));
		
		// get data from input
	   	$token       = Input::get( 'oauth_token' );
	    $verify      = Input::get( 'oauth_secret' );
	    $screen_name = DB::table('profiles')->where('oauth_token', $token)->pluck('screen_name');
	    $tweet_text  = Input::get('tweet_text');
	    //dd($token, $verify, $screen_name);
	    //$token_interface->setAccessTokenSecret($token);
/*
 $request_token = [
 			$token = Input::get( 'oauth_token' ),
	  	    $verify = Input::get( 'oauth_secret' ),
 			 Session::put('oauth_request_token', $token),
        	 Session::put('oauth_request_token_secret', $verify),
            'token'  => Session::get('oauth_request_token'),
            'secret' => Session::get('oauth_request_token_secret')


        ];

        dd($request_token);
            
*/
    }
      

    // function to store the users twitter timeline in the database
    public function insertTweet()
	{

			if(Auth::check())
			{
			
  

		        	$tweets = Twitter::getUserTimeline(array($screen_name='Auth::user()->username;', 'count' => 10, 'format' => 'object'));
				 
				    foreach($tweets as $tweet){
				        echo '<b>Tweet Text:</b> '.Twitter::linkify($tweet->text).'<br>';
				        echo '<strong>Posted By:</strong> <a href="http:'.Twitter::linkUser($tweet->user).
				        '">'.$tweet->user->name.'</a> <em>'.Twitter::ago($tweet->created_at).'</em><br>';
				        echo '<strong>Original Tweet:</strong> <a href="http:'.Twitter::linkTweet($tweet).
				        '">http:'.Twitter::linkTweet($tweet).'</a><hr>';

				        $user = Auth::user();
						//dd($user);
		            
			             $tweet = array(
			                'user_id' => Auth::user()->social_media_id,
			                'screen_name'      => Auth::user()->username,
			                'tweet_text'      => Twitter::linkify($tweet->text),
			                'posted_by'   => Twitter::linkUser($tweet->user),
			                'meta_description'   => "1",
			                'tweet_link' => Twitter::linkTweet($tweet)
			            );

			              DB::table('twitter')->insert( $tweet );
	

					}   

		
		   }
		   else
		   {
		   	return Redirect::to('user/login');
		   }	



	}
   

}



		   