<?php

class HomeController extends BaseController {



		public function getIndex()
		    {
		        // Show the page
		        return View::make('site/home/index');
		    }


	    public function getData()
	    {
	    	
			$name = Input::get('name');
			$email = Input::get('email');
			$company = Input::get('company');
			
	        // Show the page
	        return View::make('site/user/create', ['name' => $name, 'email' => $email, 'company' => $company]);
	    }



		public function connect()
		    {
		   		 $profile_checker = DB::table('profiles')->where('user_id', Auth::user()->id)->get();

                    if($profile_checker == !null)
                    {
                    	Session::flash('message', "Special message goes here");
                        return Redirect::to('/dashboard');
                    }  
                    else
                    {
                        return View::make('site.main.connect');
                    }

		    }

}
