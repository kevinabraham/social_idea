<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    # Comment Management
    Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit');
    Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit');
    Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete');
    Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete');
    Route::controller('comments', 'AdminCommentsController');

    # Blog Management
    Route::get('blogs/{post}/show', 'AdminBlogsController@getShow');
    Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit');
    Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit');
    Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete');
    Route::post('blogs/{post}/delete', 'AdminBlogsController@postDelete');
    Route::controller('blogs', 'AdminBlogsController');

    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::controller('users', 'AdminUsersController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    # Admin Dashboard
    Route::controller('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

// Home page routes
Route::get('/', function()
{
    // Return home page
    return View::make('site.home.index');
});

// Terms page
Route::get('/terms', function()
{
    // Return terms page
    return View::make('site.home.terms');
});

// Privacy page
Route::get('/privacy', function()
{
    // Return privacy page
    return View::make('site.home.privacy');
});

// Trademark page
Route::get('/trademark', function()
{
    // Return trademark page
    return View::make('site.home.trademark');
});

// guidelines page
Route::get('/guidelines', function()
{
    // Return guidelines page
    return View::make('site.home.guidelines');
});

// about page
Route::get('/about', function()
{
    // Return about us page
    return View::make('site.home.about');
});

// pricing page
Route::get('/pricing', function()
{
    // Return about us page
    return View::make('site.home.pricing');
});


Route::get('/testlogin', function()
{
    // Return about us page
    return View::make('test');
});

Route::post('/home_signup', 'HomeController@getData');


/** ------------------------------------------
 *  User Auth Routes
 *  ------------------------------------------
 */
// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');



/** ------------------------------------------
 *  Social Routes
 *  ------------------------------------------
 */

//:: Social User Account Routes ::
Route::get('/sign-in-with-twitter', 'UserController@loginWithTwitter');
Route::get('/sign-in-with-facebook', 'UserController@loginWithFacebook');
Route::get('/socialConnect', 'UserController@socialConnect');
Route::get('/twitter', 'TwitterController@twitterFeed');
Route::get('/test', 'TwitterController@tweetData');
Route::get('/twitter_post', 'TwitterController@getTweet');
Route::get('/test2', 'TwitterController@insertTweet');
//:: for posting tweets ::
Route::get('/posttweet', function()
{
    return Twitter::postTweet(array('status' => 'This is a test tweet sent directly through twitter API', 'format' => 'json'));
});

Route::get('/tweet', 'TweetsController@index');
Route::post('/tweetstore', 'TweetsController@store');
Route::get('/tweetsend', 'TweetsController@send');


Route::get('/getmytweets', function()
{
    $tweets = Twitter::getUserTimeline(array($screen_name='Auth::user()->username;', 'count' => 20, 'format' => 'object'));
 
    foreach($tweets as $tweet){
        echo '<b>Tweet Text:</b> '.Twitter::linkify($tweet->text).'<br>';
        echo '<strong>Posted By:</strong> <a href="http:'.Twitter::linkUser($tweet->user).
        '">'.$tweet->user->name.'</a> <em>'.Twitter::ago($tweet->created_at).'</em><br>';
        echo '<strong>Original Tweet:</strong> <a href="http:'.Twitter::linkTweet($tweet).
        '">http:'.Twitter::linkTweet($tweet).'</a><hr>';
    }
    
});

Route::get('/getmydms', function()
{
     //dd (
        $dm = Twitter::getDmsIn(array('count' => 5, 'format' => 'object'));
       // );
    foreach($dm as $dm){
        echo '<b>dm Text:</b> '.Twitter::linkify($dm->text).'<br>';
        echo '<b>dm ID:</b> '.$dm->sender->id.'<br>';
        echo '<b>dm Name:</b> '.$dm->sender->name.'<br>';
        echo '<b>dm Screenanme:</b> '.$dm->sender->screen_name.'<br>';
        echo '<b>dm Profile picture:</b> '.$dm->sender->profile_image_url.'<br>';

    }
    
});

Route::group(array('before' => 'auth'), function(){

Route::get('/social_media_id', 'TwitterController@uniqueProfile');
Route::get('/dashboard', 'TwitterController@dashboard');
Route::get('/connect', 'HomeController@connect');


Route::get('/connect-to-twitter', 'UserController@connectToTwitter');

});


Route::get('/profiles', function()
{
    // Return about us page
    echo Profiles::all();
});










// just executing this got the post to work to different accounts 
Route::get('twitter/login', ['as' => 'twitter.login', function(){
    // your SIGN IN WITH TWITTER  button should point to this route
    $sign_in_twitter = true;
    $force_login = false;

    // Make sure we make this request w/o tokens, overwrite the default values in case of login.
    Twitter::reconfig(['token' => '', 'secret' => '']);
    $token = Twitter::getRequestToken(route('twitter.callback'));

    if (isset($token['oauth_token_secret']))
    {
        $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

        Session::put('oauth_state', 'start');
        Session::put('oauth_request_token', $token['oauth_token']);
        Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

        return Redirect::to($url);
    }

    return Redirect::route('twitter.error');
}]);

Route::get('twitter/callback', ['as' => 'twitter.callback', function() {
    // You should set this route on your Twitter Application settings as the callback
    // https://apps.twitter.com/app/YOUR-APP-ID/settings
    if (Session::has('oauth_request_token'))
    {
        $request_token = [
            'token'  => Session::get('oauth_request_token'),
            'secret' => Session::get('oauth_request_token_secret'),
        ];

        Twitter::reconfig($request_token);

        $oauth_verifier = false;

        if (Input::has('oauth_verifier'))
        {
            $oauth_verifier = Input::get('oauth_verifier');
        }

        // getAccessToken() will reset the token for you
        $token = Twitter::getAccessToken($oauth_verifier);

        if (!isset($token['oauth_token_secret']))
        {
            return Redirect::route('twitter.login')->with('flash_error', 'We could not log you in on Twitter.');
        }

        $credentials = Twitter::getCredentials();

        if (is_object($credentials) && !isset($credentials->error))
        {
            // $credentials contains the Twitter user object with all the info about the user.
            // Add here your own user logic, store profiles, create new users on your tables...you name it!
            // Typically you'll want to store at least, user id, name and access tokens
            // if you want to be able to call the API on behalf of your users.

            // This is also the moment to log in your users if you're using Laravel's Auth class
            // Auth::login($user) should do the trick.

            Session::put('access_token', $token);
            $result = json_decode(json_encode($credentials),true);
            $profile = array(
                    'user_id'      => Auth::user()->id,
                    'signinType' => "laravel",

                    //'email'      => $result['name'] . '@twitter.org',
                    //'password'   => Hash::make($result['id']),  
                    //'confirmed'   => 1,
                    'social_media_id' => $result['id'],
                    'email' => $result['id'] . "-twitter",
                    'name' => $result['name'],
                    //'confirmation_code' => md5(microtime().Config::get('app.key')),
                    'location' => $result['location'],
                    'profile_picture' => $result['profile_image_url'],
                    'screen_name' => $result['screen_name'],
                    'social_media_type' => "twitter", 
                    'oauth_token' => $token['oauth_token'],
                    'oauth_secret' => $token['oauth_token_secret'],
                    'app_oauth_token' => Input::get( 'oauth_token' ),
                    'app_oauth_secret' => Input::get( 'oauth_verifier' ),
                    'x_auth_expires' => $token['x_auth_expires'], 
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime,
                );


$duplicateChecker = DB::table('profiles')->where('social_media_id', $result['id'])->get();
                    //var_dump($duplicateChecker);

                    if($duplicateChecker == !null)
                    {
                         DB::table('profiles')->update( $profile );
                        return Redirect::to('/dashboard')->with('flash_notice', 'Congrats! You\'ve successfully signed in!');
                    }  
                    else
                    {
                        DB::table('profiles')->insert( $profile );
                    }


                    Session::get('access_token', $token);

            return Redirect::to('/dashboard')->with('flash_notice', 'Congrats! You\'ve successfully signed in!');
        }

        return Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
    }
}]);



Route::get('/twitter/post_tweet', 'TwitterController@postTweet');

/* deprecated code. Updated code has been moved to Twitter controller. Review and delete
Route::get('fuckingtweet', function(){

        $new_token = array(
                    'oauth_token' => Input::get( 'oauth_token' ),
                    'oauth_token_secret' => Input::get( 'oauth_secret' ),
                    'user_id' => Input::get( 'social_media_id' ),
                    'screen_name' => Input::get( 'screen_name' ),
                    'x_auth_expires' => Input::get( 'x_auth_expires' )
                );
        Session::forget('access_token');
        Session::put('access_token', $new_token);
       
        dd( Twitter::postTweet(['status' => 'laravel is beautiful', 'format' => 'json']) );

    */



    //return Redirect::route('twitter.error');








Route::get('twitter/error', ['as' => 'twitter.error', function(){
    // Something went wrong, add your own error handling here
}]);

Route::get('twitter/logout', ['as' => 'twitter.logout', function(){
    Session::forget('access_token');
    return Redirect::to('/')->with('flash_notice', 'You\'ve successfully logged out!');
}]);



Route::get('/wonderlogin', function()
{
    return View::make('site.main.login');
});















# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::

# Filter for detect language
Route::when('contact-us','detectLang');

# Contact Us Static Page
Route::get('contact-us', function()
{
    // Return about us page
    return View::make('site/contact-us');
});


# Posts - Second to last set, match slug
Route::get('blog/{postSlug}', 'BlogController@getView');
Route::post('blog/{postSlug}', 'BlogController@postView');

# Index Page - Last route, no matches
Route::get('/blog', array('before' => 'detectLang','uses' => 'BlogController@getIndex'));


